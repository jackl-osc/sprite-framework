package com.sprite.framework.start;

public class StartExecption extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3660824096030767260L;

	public StartExecption(String message) {
		super(message);
	}
	
	public StartExecption(String message, Exception exception) {
		super(message, exception);
	}
}
