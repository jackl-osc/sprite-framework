package com.sprite.framework.start.container;

import com.sprite.framework.context.ContextEnvironment;
import com.sprite.framework.start.Container;
import com.sprite.utils.UtilMisc;

import java.io.*;

public class MvnCopyContainer implements Container{
	
	@Override
	public void init(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

	@Override
	public void start()  {

		try {
			// 更新jar
			downloadJars();

			String script = ContextEnvironment.getProperty("launcher.script");

			if(script.startsWith(File.separator) || script.startsWith("/")) {
				throw new RuntimeException("launcher.script must relative path");
			}

			String[] cmds = {script};

			System.out.println(UtilMisc.toList(cmds));
			ProcessBuilder builder = new ProcessBuilder(cmds);
			builder.inheritIO();
			builder.start();

			Thread.sleep(2000);

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("LuancherCopyContainer end!");
	}

	private void downloadJars() throws IOException {
		String scriptMvn = ContextEnvironment.getProperty("launcher.mvnpath");
		String outputDirectory = ContextEnvironment.getProperty("launcher.outputDirectory");
		String[] cmds = {scriptMvn, "dependency:copy-dependencies", "-DoutputDirectory="+outputDirectory};
		Process process = Runtime.getRuntime().exec(cmds);

		log(process);
	}

	private void print(InputStream inputStream) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

		String line = null;
		while((line =bufferedReader.readLine()) != null ) {
			System.out.println(line);
		}
	}


	private void log(Process process) throws IOException {
		{
			System.out.println("output->");
			print(process.getInputStream());

		}


		{
			System.out.println("error->");
			print(process.getErrorStream());
		}
	}

}
