package com.sprite.framework.start;

public interface Container {
    
	void init(String[] args);
	
    /**
     * start.
     */
    void start();
    
    /**
     * stop.
     */
    void stop();

}