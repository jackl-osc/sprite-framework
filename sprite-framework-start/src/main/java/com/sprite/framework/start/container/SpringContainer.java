package com.sprite.framework.start.container;

import com.sprite.framework.context.ContextEnvironment;
import com.sprite.framework.start.Container;
import com.sprite.utils.UtilString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

/**
 * @author Jack
 */
public class SpringContainer implements Container {

	private static final Logger logger = LoggerFactory.getLogger(SpringContainer.class);

	public static final String DEFAULT_SPRING_CONFIG = "classpath*:/spring-context-*.xml";

	private static ClassPathXmlApplicationContext context;

	public static ClassPathXmlApplicationContext getContext() {
		return context;
	}

	public void start() {
		String configPath = ContextEnvironment.getProperty("spring.configLocations");

		if (UtilString.isBlank(configPath)) {
			configPath = DEFAULT_SPRING_CONFIG;
		}
		
		logger.info("spring configLocations:{}", configPath);
		
		context = new ClassPathXmlApplicationContext(configPath.split("[,\\s]+"));
		context.start();
	}

	public void stop() {
		try {
			if (context != null) {
				context.stop();
				context.close();
				context = null;
			}
		} catch (Throwable e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void init(String[] args) {
		
	}
}
