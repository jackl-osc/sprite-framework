package com.sprite.framework.entity.condition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.sprite.framework.entity.EntityCondition;
import com.sprite.utils.UtilMisc;


/**
 * @author Jack
 *
 */
public class EntityFieldMap extends EntityConditionList<EntityExpr>{
	private Map<String, ?> fieldMap ;

	public static <V> List<EntityExpr> makeConditionList(Map<String, V> fieldMap, EntityComparisonOperator<?,?> op) {
		if(fieldMap == null){
			return UtilMisc.cast(Collections.EMPTY_LIST);
		}

		List<EntityExpr> list = new ArrayList<EntityExpr>(fieldMap.size());
		for(Map.Entry<String, V> entry : fieldMap.entrySet()){
			EntityExpr expr = EntityCondition.makeCondition(entry.getKey(), op, entry.getValue());
			list.add(expr);
		}

		return list;
	}

	public void init(Map<String, ?> fieldMap,
			EntityComparisonOperator<?, ?> op, EntityJoinOperator joinOperator) {
		this.fieldMap =  fieldMap == null ? Collections.<String, Object>emptyMap():fieldMap;
		EntityComparisonOperator<?,?> operator = UtilMisc.cast(op);
		super.init(joinOperator, EntityFieldMap.makeConditionList(fieldMap, operator));
	}

	public Object getField(String name) {
		return this.fieldMap.get(name);
	}

}
