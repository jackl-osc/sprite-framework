package com.sprite.framework.entity.mapper.reolver.parser;

import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.Parser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.framework.entity.mapper.reolver.node.TrimSqlNode;
import com.sprite.utils.UtilXml;

public class TrimNodeParser implements NodeParser {

	public String tagName() {
		return "trim";
	}

	public SqlNode parser(Element element) {
		TrimSqlNode sqlNode = new TrimSqlNode();
		List<String> params = new LinkedList<String>();
		
		sqlNode.setPrefix(Parser.parserTextContent(element.getAttribute("prefix"), params));
		sqlNode.setPrefixOverride(element.getAttribute("prefixOverride"));
		sqlNode.setSuffix(Parser.parserTextContent(element.getAttribute("suffix"), params));
		sqlNode.setSuffixOverride(element.getAttribute("suffixOverride"));
		
		sqlNode.setContent(Parser.parserMix(UtilXml.childNodeList(element)));
		return sqlNode;
	}

}
