package com.sprite.framework.entity.model;

/**
 * 实体字段类型
 * @author Jack
 */
public class ModelFieldType {
	private String type;
	private String sqlType;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSqlType() {
		return sqlType;
	}
	public void setSqlType(String sqlType) {
		this.sqlType = sqlType;
	}
}
