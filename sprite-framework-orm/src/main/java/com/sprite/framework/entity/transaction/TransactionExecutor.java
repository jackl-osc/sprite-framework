package com.sprite.framework.entity.transaction;

/**
 * 执行器
 */
@FunctionalInterface
public interface TransactionExecutor {
	void execute();
}
