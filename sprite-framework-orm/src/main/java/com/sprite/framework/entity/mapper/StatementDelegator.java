package com.sprite.framework.entity.mapper;

import java.util.List;

import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.mapper.reolver.NodeParser;

/**
 * statement
 * @author Jack
 *
 * @see DefaultStatementDelegator
 */
public interface StatementDelegator {

	StatementScript makeScript(String statement, Object param);

	DataScriptStatement resolve(String statement, Object param);

	/**
	 * 添加节点解析器
	 * @param parsers 解析器
	 */
	void addNodeParsers(List<NodeParser> parsers);
}
