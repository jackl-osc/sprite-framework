package com.sprite.framework.entity.util;

import com.sprite.framework.exception.ErrorMessage;
import com.sprite.framework.entity.EntityObject;

public class EntityObjectUtil {

	public static void checkMustField(EntityObject entityObject,ErrorMessage errorMessage, String... fields) {
		if(fields == null || fields.length == 0) {
			return;
		}
		
		for(String field : fields) {
			if(entityObject.get(field) == null) {
				errorMessage.addMessage("field ["+field+"] is null");
			}
		}
	}
}
