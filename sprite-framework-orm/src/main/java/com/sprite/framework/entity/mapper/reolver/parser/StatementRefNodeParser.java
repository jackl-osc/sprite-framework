package com.sprite.framework.entity.mapper.reolver.parser;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.framework.entity.mapper.reolver.node.StatementRefSqlNode;

public class StatementRefNodeParser implements NodeParser{

	public String tagName() {
		return "statement";
	}

	public SqlNode parser(Element element) {
		String id = element.getAttribute("ref");
		return new StatementRefSqlNode(id);
	}

}
