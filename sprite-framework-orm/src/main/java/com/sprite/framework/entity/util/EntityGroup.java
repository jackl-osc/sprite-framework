package com.sprite.framework.entity.util;

import java.util.LinkedList;
import java.util.List;

import com.sprite.framework.entity.EntityObject;

/**
 * 实体数据组，同一组内的数据会在一个事务下提交
 * @author Jack
 *
 */
public class EntityGroup {

	private List<EntityObject> entities = new LinkedList<>();
	
	public void addEntity(EntityObject entity){
		entities.add(entity);
	}
	
	public void addEntities(List<EntityObject>  entities){
		this.entities.addAll(entities);
	}
	
	public List<EntityObject> getEntities(){
		return entities;
	}
}
