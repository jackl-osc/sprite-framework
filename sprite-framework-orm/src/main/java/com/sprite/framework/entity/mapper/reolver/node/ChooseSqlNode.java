package com.sprite.framework.entity.mapper.reolver.node;

import java.util.LinkedList;
import java.util.List;

import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

public class ChooseSqlNode implements SqlNode{

	private List<SqlNode> nodeList = new LinkedList<SqlNode>();
	private SqlNode otherwise;
	
	public boolean apply(DynamicContext context) {
		for(SqlNode sqlNode : nodeList){
			if(sqlNode.apply(context)){
				return true;
			}
		}
		
		if(otherwise != null){
			otherwise.apply(context);
			return true;
		}
		
		return false;
	}
	
	public void appendSqlNode(SqlNode sqlNode){
		nodeList.add(sqlNode);
	}
	
	public void setOtherwise(SqlNode sqlNode){
		otherwise = sqlNode;
	}

}
