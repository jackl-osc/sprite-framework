package com.sprite.framework.entity;

/**
 * 预留关键字
 * @author Jack
 *
 */
public final class FieldConstancts {
	
	// 创建时间
	public static final String CREATED_STAMP = "createdStamp";
	
	// 更新时间
	public static final String UPDATED_STAMP = "updatedStamp";
	
	/**
	 * 逻辑删除
	 */
	public static final String DELETED = "deleted";
	
	private FieldConstancts(){}
}
