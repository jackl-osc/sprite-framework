package com.sprite.framework.entity.mapper.reolver;

import org.w3c.dom.Element;

public interface NodeParser {

	String tagName();
	
	SqlNode parser(Element element);
}
