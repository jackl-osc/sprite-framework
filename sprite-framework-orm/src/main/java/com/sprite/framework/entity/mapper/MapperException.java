package com.sprite.framework.entity.mapper;

import com.sprite.framework.exception.GeneralRuntimeException;

public class MapperException extends GeneralRuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8718570161215793688L;

	public MapperException() {
		super();
	}

	public MapperException(String message, Throwable nested) {
		super(message, nested);
	}

	public MapperException(String message) {
		super(message);
	}

	public MapperException(Throwable nested) {
		super(nested);
	}

	
}
