package com.sprite.framework.entity;

import java.util.List;

import com.sprite.framework.entity.transaction.TransactionScript;
import com.sprite.framework.entity.util.DatabaseUtil;

/**
 * Sql 执行器
 * @author Jack
 */
public interface EntityScriptExecutor {

	/**
	 * 执行
	 * @param script
	 * @return
	 */
	int execute(EntityScript script);

	/**
	 * 查询
	 * @param script
	 * @param clazz
	 * @param <T>
	 * @return
	 */
	<T> List<T> query(EntityScript script, Class<T> clazz);

	/**
	 * 开启事务
	 * @param create
	 * @return
	 */
	TransactionScript beginTransaction(boolean create);

	/**
	 * 获取数据库工具
	 * @return
	 */
	DatabaseUtil getDatabaseUtil();
}
