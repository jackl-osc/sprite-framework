package com.sprite.framework.entity;

import java.util.LinkedList;
import java.util.List;

/**
 * 数据脚本
 * @author Jack
 *
 */
public class DataScriptStatement implements EntityScript{
	private StringBuilder script = new StringBuilder();
	private List<Object> params = new LinkedList<>();
	
	public DataScriptStatement append(String scriptFragment){
		if(scriptFragment == null){
			return this;
		}
		script.append(scriptFragment).append(" ");
		return this;
	}
	
	public DataScriptStatement addParams(List<Object> params){
		this.params.addAll(params);
		return this;
	}
	
	public DataScriptStatement addParam(Object param){
		this.params.add(param);
		return this;
	}
	
	public List<Object> getParams(){
		return new LinkedList<Object>(params);
	}
	
	public String toScriptString(){
		return script.toString();
	}
	
	@Override
	public String toString() {
		return script.toString();
	}

	@Override
	public DataScriptStatement getStatement() {
		return this;
	}
	
	public void clear() {
		script.setLength(0);
		params.clear();
	}
	
}
