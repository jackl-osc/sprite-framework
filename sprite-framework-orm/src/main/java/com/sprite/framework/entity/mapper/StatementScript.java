package com.sprite.framework.entity.mapper;

import com.sprite.framework.entity.EntityFindOptions;
import com.sprite.framework.entity.EntityScript;
import com.sprite.framework.entity.DataScriptStatement;

public class StatementScript implements EntityScript{

	private String statement;
	private StatementDelegator delegator;
	private Object param;
	private EntityFindOptions options;
	public StatementScript(String statement, StatementDelegator delegator){
		this.statement = statement;
		this.delegator = delegator;
	}
	
	public Object getParam() {
		return param;
	}

	public void setParam(Object param) {
		this.param = param;
	}

	public DataScriptStatement getStatement() {
		return delegator.resolve(statement, param);
	}

	public EntityFindOptions options() {
		return options;
	}

	public void setOptions(EntityFindOptions options) {
		this.options = options;
	}

}
