package com.sprite.framework.entity.model;

/**
 * 实体视图
 * @author Jack
 */
public interface ModelEntityView {

	/**
	 * 是否包含指定的字段
	 * @param field 字段
	 * @return 是否有字段
	 */
	boolean hasField(String field);

	/**
	 * 	解析 entityAlias.field 返回实际的 entityAlis.column
	 * @param fieldPath entityAlias.field
	 * @return 解析后的字符串
	 */
	String resolveFieldPath(String fieldPath);

}
