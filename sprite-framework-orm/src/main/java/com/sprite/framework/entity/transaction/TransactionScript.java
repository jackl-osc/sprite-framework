package com.sprite.framework.entity.transaction;

/**
 * 事务脚本
 * @author Jack
 */
public interface TransactionScript {
	/**
	 * 开启事务
	 */
	void begin();

	/**
	 * 事务回滚
	 */
	void rollback();

	/**
	 * 事务提交
	 */
	void commit();

	/**
	 * <p>事务执行:</p>
	 * <p>1、开启事务 {@link #begin()}</p>
	 * <p>2、执行 {@link TransactionExecutor#execute()}</p>
	 * <p>2、提交或回滚 {@link #commit()} 或 {@link #rollback()}</p>
	 * @param executor 执行
	 */
	void execute(TransactionExecutor executor);
}
