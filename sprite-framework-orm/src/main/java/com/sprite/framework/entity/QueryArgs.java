package com.sprite.framework.entity;

import java.util.List;
import java.util.Set;

public class QueryArgs {
	public EntityCondition whereCondition;
	public Set<String> queryFields;
	public List<String> orderBy;
	public EntityFindOptions findOptions;
}
