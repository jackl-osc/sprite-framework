package com.sprite.framework.entity.condition;

import java.util.List;

import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.EntityCondition;
import com.sprite.framework.entity.model.ModelEntityView;


/**
 * <p>条件列表</p>
 * 条件列表由 一个连接操作符和多个实体条件组成，实体条件之间通过连接操作符连接
 * @author Jack
 *
 * @see EntityCondition
 * @see EntityJoinOperator
 */
public class EntityConditionList<T extends EntityCondition>  implements EntityCondition{
	protected List<T> conditionList;
	protected EntityJoinOperator operator;

	public EntityConditionList(){}

	public EntityConditionList(EntityJoinOperator operator, List<T> conditionList) {
		init(operator, conditionList);
	}

	public void init(EntityJoinOperator operator, List<T> conditionList){
		if(operator == null){
			throw new IllegalArgumentException("The operator argument cannot be null");
		}
		this.conditionList = conditionList;
		this.operator = operator;
	}


	@Override
	public void makeScript(DataScriptStatement script, ModelEntityView modelViewEntity) throws EntityException {
		operator.makeScript(script, conditionList, modelViewEntity);
	}


}
