package com.sprite.framework.entity.config;

import com.sprite.framework.component.ConfigHandler;
import com.sprite.framework.component.SpriteConfig;
import org.w3c.dom.Element;

/**
 * 实体资源加载
 * @author Jack
 */
public class EntityResourceHandler implements ConfigHandler{

	@Override
	public void handle(Element element, SpriteConfig spriteConfig) {
		
		String type = element.getAttribute("type");
		String location = spriteConfig.relativeResource(element.getAttribute("location")).toString();
		EntityResourceInfo entityResourceInfo = new EntityResourceInfo(location, type);
		entityResourceInfo.setName(element.getAttribute("name"));
		if("fieldType".equals(type)) {
			EntityResourceInfo.addFieldType(entityResourceInfo);
		}else if("model".equals(type)) {
			EntityResourceInfo.addEntityModel(entityResourceInfo);
		}else if("data".equals(type)) {
			EntityResourceInfo.addEntityData(entityResourceInfo);
		}
	}
}
