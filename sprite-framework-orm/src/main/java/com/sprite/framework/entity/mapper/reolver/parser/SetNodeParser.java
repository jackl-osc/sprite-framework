package com.sprite.framework.entity.mapper.reolver.parser;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.Parser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.framework.entity.mapper.reolver.node.TrimSqlNode;
import com.sprite.utils.UtilXml;

public class SetNodeParser implements NodeParser{

	public SqlNode parser(Element element) {
		TrimSqlNode sqlNode = new TrimSqlNode();
		sqlNode.setContent(Parser.parserMix(UtilXml.childNodeList(element)));
		sqlNode.setPrefix("set");
		return sqlNode;
	}

	public String tagName() {
		return "set";
	}
}
