package com.sprite.framework.entity;

import java.util.List;
import java.util.Map;

/**
 * @author Jack
 *
 */
public interface  Delegator {
	

	/**
	 * <p>保存数据</p>
	 * 如果实体类继承了{@link SequencedEntity},那么当实体没有设置主键时，会自动从序列中获取一个新的值作为主键。
	 *
	 * @param entityName 实体名称
	 * @param entity IdentifyEntity implements
	 * @param <T> 泛型
	 * @throws EntityException  实体错误
	 */
	<T> void store(String entityName, T entity) throws EntityException;
	
	/**
	 * <p>批量保存数据</p>
	 * 如果实体类继承了{@link SequencedEntity},那么当实体没有设置主键时，会自动从序列中获取一个新的值作为主键。
	 * 
	 * @param entityName 实体名称
	 * @param entities 实体
	 * @param <T> 泛型
	 * @throws EntityException  实体错误
	 */
	<T> void storeAll(String entityName, List<T> entities) throws EntityException;
	
	/**
	 * 更新
	 * @param entityName
	 * 					实体名称
	 * @param fieldsToUpdate
	 * 					更新的字段及新的值
	 * @param condition
	 * 					条件
	 * @throws EntityException 实体错误
	 */
	void update(String entityName, Map<String, Object> fieldsToUpdate, EntityCondition condition) throws EntityException;

	
	/**
	 * 获取唯一记录值
	 * @param entityName 实体名称
	 * @param entityCondition 查询条件
	 * @param clazz 实体类型
	 * @param <T> 泛型
	 * @return 对象
	 * @throws EntityException 实体错误
	 */
	<T> T findOne(String entityName, EntityCondition entityCondition, Class<T> clazz) throws EntityException;
	
	/**
	 * 获取唯一记录值
	 * @param entityName 实体名称
	 * @param entityCondition 查询条件
	 * @return 对象
	 * @throws EntityException 实体错误
	 */
	EntityObject findOne(String entityName, EntityCondition entityCondition) throws EntityException;

	
	/**
	 * <p>移除</p>
	 * 如果实体类实现接口 {@link LogiclyDeletable}则逻辑删除
	 * 
	 * @param entityName 实体名称
	 * @param EntityCondition 条件
	 */
	void remove(String entityName, EntityCondition EntityCondition) throws EntityException;

	/**
	 * <p>清空所有实体数据</p>
	 * 如果实体类实现接口 {@link LogiclyDeletable}则逻辑删除
	 * 
	 * @param entityName 实体名称
	 */
	void remove(String entityName) throws EntityException;
	
	/**
	 * 
	 * @param entityName 实体名称
	 * @param queryArgs	查询参数
	 * @param clazz	类型
	 * @param <T> 泛型
	 * @return	实体列表
	 * @throws EntityException 查询异常
	 */
	<T> List<T> findList(String entityName, QueryArgs queryArgs, Class<T> clazz) throws EntityException;

	/**
	 *
	 * @param entityName 实体名称
	 * @param queryArgs	查询参数
	 * @return	实体列表
	 * @throws EntityException 查询异常
	 */
	List<EntityObject> findList(String entityName, QueryArgs queryArgs) throws EntityException;

	/**
	 *
	 * @param entityName 实体名称
	 * @param whereCondition 查询条件
	 * @param clazz 类型
	 * @param <T> 泛型
	 * @return 泛型列表
	 * @throws EntityException 查询异常
	 */
	<T> List<T> findList(String entityName, EntityCondition whereCondition, Class<T> clazz) throws EntityException;

	/**
	 *
	 * @param entityName 实体名称
	 * @param whereCondition	查询条件
	 * @return 数据列表
	 * @throws EntityException 查询异常
	 */
	List<EntityObject> findList(String entityName, EntityCondition whereCondition) throws EntityException;

	/**
	 * Count the entities by EntityCondition
	 * @param entityName 实体名称
	 * @param entityCondition	查询条件
	 * @return 记录数
	 * @throws EntityException 查询异常
	 */
	long count(String entityName, EntityCondition entityCondition) throws EntityException;
	
	/**
	 * @param script 查询脚本
	 * @return 记录数
	 * @throws EntityException 查询异常
	 */
	long count(EntityScript script) throws EntityException;
	
	
	/**
	 * 查询记录的指定属性列, 并转为对象
	 * @param script 查询脚本
	 * @param clazz 实体类型
	 * @param <T> 实体泛型
	 * @return 实体
	 * @throws EntityException 查询异常
	 */
	<T> T findOne(EntityScript script, Class<T> clazz) throws EntityException;

	/**
	 * @param script 查询脚本
	 * @return 实体
	 * @throws EntityException 查询异常
	 */
	EntityObject findOne(EntityScript script) throws EntityException;
	
	/**
	 * 查询多条记录的指定属性列, 并转为对象
	 * @param script 查询脚本
	 * @param clazz 实体类型
	 * @param <T> 实体泛型
	 * @return 实体列表
	 * @throws EntityException 查询异常
	 */
	<T> List<T> findList(EntityScript script, Class<T> clazz) throws EntityException;
	
	/**
	 * @param script 查询脚本
	 * @return 实体列表
	 * @throws EntityException 查询异常
	 */
	List<EntityObject> findList(EntityScript script) throws EntityException;


	EntityObject findOne(String entityName, QueryArgs queryArgs)  throws EntityException;

	<T>T findOne(String entityName, QueryArgs queryArgs, Class<T> clazz)  throws EntityException;
}
