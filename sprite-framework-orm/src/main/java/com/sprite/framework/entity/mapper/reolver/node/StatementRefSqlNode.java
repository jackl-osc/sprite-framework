package com.sprite.framework.entity.mapper.reolver.node;

import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.mapper.DefaultStatementDelegator;
import com.sprite.framework.entity.mapper.StatementMapper;
import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

/**
 * @author Jack
 */
public class StatementRefSqlNode implements SqlNode{

	private String ref;

	public StatementRefSqlNode(String ref) {
		super();
		this.ref = ref;
	}

	public boolean apply(DynamicContext context) {
		StatementMapper statementMapper = DefaultStatementDelegator.DELEGATOR_INS.getMapper(ref);
		if(statementMapper == null) {
			statementMapper =  DefaultStatementDelegator.DELEGATOR_INS.getMapper(context.getNamespace()+"."+ ref);
		}

		if(statementMapper == null) {
			throw new EntityException("not found statement ref: "+ref);
		}
		
		DataScriptStatement dataScript = statementMapper.mapper(context.getBindings());
		context.append(dataScript.toScriptString());
		context.addParams(dataScript.getParams());
		return true;
	}

}
