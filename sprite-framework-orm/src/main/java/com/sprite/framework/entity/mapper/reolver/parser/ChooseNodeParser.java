package com.sprite.framework.entity.mapper.reolver.parser;

import java.util.List;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.Parser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.framework.entity.mapper.reolver.node.ChooseSqlNode;
import com.sprite.utils.UtilXml;

public class ChooseNodeParser implements NodeParser{

	public String tagName() {
		return "choose";
	}

	public SqlNode parser(Element element) {
		ChooseSqlNode sqNode = new ChooseSqlNode();
		
		List<Element> list = UtilXml.childElementList(element, "when");
		
		for(Element el : list){
			sqNode.appendSqlNode(Parser.parser(el));
		}
		
		Element oElement = UtilXml.childElementOne(element, "otherwise");
		if(oElement != null){
			sqNode.setOtherwise(Parser.parser(oElement));
		}
		
		return sqNode;
	}

}
