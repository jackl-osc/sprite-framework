package com.sprite.framework.entity.condition;

import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.EntityCondition;
import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.model.ModelEntityView;

/**
 * 不做任何操作
 * @author Jack
 */
public final class EmptyCondition implements EntityCondition {

    private static volatile EmptyCondition instance;

    public static EmptyCondition getInstance(){
        if(instance == null){
            synchronized (EmptyCondition.class){
                if(instance == null){
                    instance = new EmptyCondition();
                }
            }
        }

        return instance;
    }

    @Override
    public void makeScript(DataScriptStatement script, ModelEntityView modelViewEntity) throws EntityException {

    }

    private EmptyCondition(){}
}
