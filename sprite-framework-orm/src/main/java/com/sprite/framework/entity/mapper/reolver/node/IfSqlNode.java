package com.sprite.framework.entity.mapper.reolver.node;

import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.OgnlUtils;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

/**
 * if、when
 * @author Jack
 *
 */
public class IfSqlNode implements SqlNode{

	private String test;
	
	private SqlNode sqlNode;
	
	public boolean apply(DynamicContext context) {
		if(OgnlUtils.evaluateBoolean(test, context.getBindings())){
			sqlNode.apply(context);
			return true;
		}
		
		return false;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public SqlNode getSqlNode() {
		return sqlNode;
	}

	public void setSqlNode(SqlNode sqlNode) {
		this.sqlNode = sqlNode;
	}
	
}
