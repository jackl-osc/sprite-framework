package com.sprite.framework.entity.condition;

import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.model.ModelEntityView;

/**
 * 多个操作值
 * @author Jack
 * @see com.sprite.framework.entity.EntityCondition#in(String, MultiOperateValue)
 * @see com.sprite.framework.entity.script.EntityView
 * @see CollectionOperateValue
 *
 */
public interface MultiOperateValue {
	void makeStatement(DataScriptStatement statement, ModelEntityView modelViewEntity) throws EntityException;
}
