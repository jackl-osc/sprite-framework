package com.sprite.framework.entity.config;

import java.util.HashMap;
import java.util.Map;

public class EntityConfig {

	// key is name
	private Map<String, FieldType> modelFieldTypes = new HashMap<>();

	public FieldType getFieldType(String name) {
		return modelFieldTypes.get(name);
	}

	public void addFieldType(FieldType fieldType) {
		modelFieldTypes.put(fieldType.getName(), fieldType);
	}
}
