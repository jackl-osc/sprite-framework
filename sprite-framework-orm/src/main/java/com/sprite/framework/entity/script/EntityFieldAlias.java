package com.sprite.framework.entity.script;

import com.sprite.framework.entity.model.ModelField;

/**
 * 实体属性别名
 * @author Jack
 */
public final class EntityFieldAlias {
	private String entityAlias;
	private String fieldName;
	private String fieldAlias;
	private EntityScriptFunction function;	// 函数
	private Boolean isASC;   	// 是否升序
	
	private ModelField modelField;
	
	protected EntityFieldAlias(String entityAlias, String fieldName, String fieldAlias, EntityScriptFunction function, Boolean isASC) {
		this.entityAlias = entityAlias;
		this.fieldName = fieldName;
		this.fieldAlias = fieldAlias;
		this.function = function;
		this.isASC = isASC;
	}
	
	public String getEntityAlias() {
		return entityAlias;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFieldAlias() {
		return fieldAlias;
	}

	public EntityScriptFunction getFunction() {
		return function;
	}

	public Boolean getIsASC() {
		return isASC;
	}
	
	public void setIsASC() {
		this.isASC = true;
	}

	public String getFieldPath(){
		return entityAlias + "." + fieldName;
	}

	public ModelField getModelField() {
		return modelField;
	}

	public void setModelField(ModelField modelField) {
		this.modelField = modelField;
	}

	public void setFieldAlias(String fieldAlias) {
		this.fieldAlias = fieldAlias;
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}
	
	public String function(String field) {
		if(function != null) {
			return function.function(field);
		}
		return field;
	}
	
}
