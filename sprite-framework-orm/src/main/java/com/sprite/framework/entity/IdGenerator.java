package com.sprite.framework.entity;

/**
 * ID生成器
 * <p>生成器应该具备以下特性：</p>
 * <p>1、应该是线程安全的</p>
 * <p>2、保证生成ID唯一，不可重复</p>
 * @author Jack
 */
public interface IdGenerator {

	/**
	 * 生成器名称
	 * @return 名称
	 */
	String name();

	/**
	 * 生成ID
	 * @param entityName 实体名称
	 * @return 自增值
	 */
	String nextId(String entityName);
}
