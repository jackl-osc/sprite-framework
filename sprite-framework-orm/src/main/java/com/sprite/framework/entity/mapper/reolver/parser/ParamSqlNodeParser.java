package com.sprite.framework.entity.mapper.reolver.parser;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.framework.entity.mapper.reolver.node.ParamSqlNode;

public class ParamSqlNodeParser implements NodeParser{

	public String tagName() {
		return "param";
	}

	public SqlNode parser(Element element) {
		ParamSqlNode node = new ParamSqlNode();
		node.setExpression(element.getAttribute("expression"));
		return node;
	}

}
