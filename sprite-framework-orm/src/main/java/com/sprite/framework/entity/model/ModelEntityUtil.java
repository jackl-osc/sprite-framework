package com.sprite.framework.entity.model;

import java.util.Collection;
import java.util.List;

import com.sprite.framework.entity.EntityException;
import com.sprite.utils.cache.UtilCache;


/**
 * @author Jack
 *
 */
public final class ModelEntityUtil{

	// 数据库类型
	public static String DATABASE_TYPE_SQLSERVER = "sqlserver";
	public static String DATABASE_TYPE_SQLSERVER2012 = "sqlserver2012";
	public static String DATABASE_TYPE_MYSQL = "mysql";
	public static String DATABASE_TYPE_ORACLE = "oracle";
	public static String DATABASE_TYPE_PGSQL = "pgsql";
	
	/**
	 * key is entityName
	 */
	private static UtilCache<ModelEntity> persistentClasses = UtilCache.createUtilCache("cache.persistentClasses");

	/**
	 * 自增序列实体类
	 */
	private static ModelEntity sequenceEntity = new ModelEntity();
	
	static{
		sequenceEntity.setEntityName("SequenceItem");
		sequenceEntity.addModelField(new ModelField("id", "id", "str-short"));
		sequenceEntity.addModelField(new ModelField("sequenceValue", "sequence_value", "int-long"));
	}
	
	/**
	 * database type
	 */
	private static String databaseType;
	
	/**
	 * key is type
	 */
	private static UtilCache<ModelFieldType> modelFieldTypes = UtilCache.createUtilCache("_modelFeildType_");

	protected static void addModelEntities(Collection<ModelEntity> modelEntities){
		if(modelEntities == null){
			return;
		}

		for(ModelEntity modelEntity : modelEntities){
			String entityName = modelEntity.getEntityName();
			if(persistentClasses.putIfAbsent(entityName, modelEntity) != null) {
				throw new EntityException("EntityModel has existed ["+entityName+"]");
			}
		}
	}
	
	protected static void addModelEntity(ModelEntity modelEntity){
		if(modelEntity == null){
			return;
		}

		if(persistentClasses.putIfAbsent(modelEntity.getEntityName(), modelEntity) != null) {
			throw new EntityException("EntityModel has existed ["+modelEntity.getEntityName()+"]");
		}
	}

	protected static void addModelFieldType(Collection<ModelFieldType> fieldTypes){
		if(fieldTypes == null){
			return;
		}

		for(ModelFieldType modelEntity : fieldTypes){
			String type = modelEntity.getType();
			if(modelFieldTypes.putIfAbsent(type, modelEntity) != null) {
				throw new EntityException("ModelFieldType has existed ["+type+"]");
			}
		}
	}

	protected static void addModelFieldType(ModelFieldType fieldType){
		String type = fieldType.getType();
		if(modelFieldTypes.putIfAbsent(type, fieldType) != null) {
			throw new EntityException("ModelFieldType has existed ["+type+"]");
		}
	}

	protected static void setDatabaseType(String type){
		ModelEntityUtil.databaseType = type;
	}
	
	public static String getDatabaseType() {
		return databaseType;
	}

	public static ModelFieldType getFieldType(String type) {
		ModelFieldType fieldType = modelFieldTypes.get(type);
		if(fieldType == null){
			throw new EntityException("not found ModelFieldType by ["+type+"]");
		}

		return fieldType;
	}

	public static ModelEntity getModelEntity(String entityName) throws EntityException{
		ModelEntity modelEntity= persistentClasses.get(entityName);
		if(modelEntity == null){
			throw new EntityException("not found EntityModel by ["+entityName+"]");
		}
		return modelEntity;
	}
	
	public static List<ModelEntity> getModelEntityList(){
		return persistentClasses.values();
	}

}
