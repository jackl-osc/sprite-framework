package com.sprite.framework.entity.util;

import com.sprite.framework.entity.EntityException;
import com.sprite.utils.conversion.ConversionException;
import com.sprite.utils.conversion.Converts;

public class EntityUtil {

	public static <T> T convert(Object value, Class<T> targetClass){
		try {
			return Converts.convert(value, targetClass);
		} catch (ConversionException e) {
			throw new EntityException(e);
		}
	}
}
