package com.sprite.framework.entity.model;

/**
 * 实体字段
 * @author Jack
 */
public class ModelField {

	private String fieldName;
	private String colName;
	private String type;
	private boolean key = false;
	
	public ModelField(String fieldName, String colName, String type, boolean isKey) {
		this.fieldName = fieldName;
		this.colName = colName;
		this.type = type;
		this.key = isKey;
	}
	
	public ModelField(String fieldName, String colName, String type) {
		this.fieldName = fieldName;
		this.colName = colName;
		this.type = type;
	}
	public String getFieldName() {
		return fieldName;
	}
	public String getColName() {
		return colName;
	}
	public boolean isKey() {
		return key;
	}
	public void setKey(boolean key) {
		this.key = key;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public String getType() {
		return type;
	}
	
}
