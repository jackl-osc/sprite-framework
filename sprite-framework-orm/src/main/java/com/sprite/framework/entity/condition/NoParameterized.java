package com.sprite.framework.entity.condition;

/**
 * 非参数化表现
 * @author Jack
 *
 */
public final class NoParameterized{
	
	public static final NoParameterized NULL = new NoParameterized("NULL", false);
	
	private Object param;
	private boolean openQuote;
	
	public NoParameterized(Object param) {
		this(param, true);
	}

	public NoParameterized(Object param, boolean openQuote) {
		this.openQuote = openQuote;
		if(param != null){
			boolean isValid = param instanceof CharSequence || param instanceof Number;
			if(!isValid){
				throw new IllegalArgumentException("Only The Number, CharSequence, NULL can be no parameterized");
			}
			
			this.param = param;
		}
	}

	public boolean isOpenQuote() {
		return openQuote;
	}

	public Object getParam() {
		return param;
	}

	public String safeString(){
		if(param == null){
			return "null";
		}
		
		if(param instanceof CharSequence){
			if(openQuote){
				return "'"+param+"'";
			}else{
				return param.toString();
			}
		}else{
			return String.valueOf(param);
		}
	}
	
	@Override
	public String toString() {
		if(param == null){
			return "null";
		}
		
		if(param instanceof CharSequence){
			if(openQuote){
				return "'"+param+"'";
			}else{
				return param.toString();
			}
		}else{
			return String.valueOf(param);
		}
	}
	
}
