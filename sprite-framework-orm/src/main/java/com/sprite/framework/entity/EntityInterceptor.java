package com.sprite.framework.entity;

import java.util.Map;

/**
 * 实体拦截器，用于改变实体的值
 * @see EntityInterceptorAdapter
 */
public interface EntityInterceptor {

	/**
	 * key is field name
	 * @param entityFields 实体信息
	 */
	void save(Map<String, Object> entityFields);
	
	
	/**
	 * key is field name
	 * @param entityFields 实体信息
	 */
	void update(Map<String, Object> entityFields);
}
