package com.sprite.framework.entity.mapper.reolver;

/**
 * Sql 节点
 *
 * @author Jack
 */
public interface SqlNode {
	boolean apply(DynamicContext context);
}
