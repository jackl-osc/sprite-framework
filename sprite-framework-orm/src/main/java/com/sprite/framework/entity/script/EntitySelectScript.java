package com.sprite.framework.entity.script;


import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.EntityCondition;
import com.sprite.framework.entity.EntityFindOptions;
import com.sprite.framework.entity.EntityScript;

/**
 * 查询脚本
 * @author Jack
 *
 */
public final class EntitySelectScript implements EntityScript{

	private EntityView entityView;

	private String entityAlias = "a";
	
	public EntitySelectScript(String entityName) {
		entityView = new EntityView(entityName, entityAlias);
	}

	protected EntitySelectScript() {
	}

	public void distinct(){
		entityView.distinct();
	}

	public void addSelectedField(String field, String fieldAlias){
		entityView.addViewFieldAs(entityAlias, field, fieldAlias);
	}

	public void addSelectedField(String field){
		entityView.addViewField(entityAlias, field);
	}
	
	public void addSelectedField(String... fields){
		entityView.addViewField(entityAlias, fields);
	}
	
	public void addSelectedField(String field, EntityScriptFunction function){
		entityView.addViewField(entityAlias, field, function);
	}
	
	public void addGroupByField(String field){
		entityView.addGroupByField(entityAlias, field);
	}

	public void selectAllField() {
		entityView.addViewField(entityAlias);
	}
	
	/**
	 * 添加排序字段
	 * @param field 属性
	 */
	public void addOrderByDesc(String field){
		entityView.addOrderByDesc(entityAlias, field);
	}
	
	/**
	 * 添加排序字段
	 * @param field 属性
	 */
	public void addOrderByAsc(String field){
		entityView.addOrderByAsc(entityAlias, field);
	}

	public void setWhereCondition(EntityCondition whereCondition){
		entityView.setWhereCondition(whereCondition);
	}

	public void setHavingCondition(EntityCondition havingCondition) {
		entityView.setHavingCondition(havingCondition);
	}

	public void setOptions(EntityFindOptions options) {
		entityView.setOptions(options);
	}
	
	public EntityScript countScript(String field){
		return entityView.count(entityAlias, field);
	}

	public EntityScript countScript(){
		return entityView.countScript();
	}
	
	@Override
	public DataScriptStatement getStatement() {
		DataScriptStatement statement = new DataScriptStatement();
		entityView.makeStatement(statement);
		return statement;
	}

}
