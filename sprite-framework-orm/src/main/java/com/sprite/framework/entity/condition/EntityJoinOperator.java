package com.sprite.framework.entity.condition;

import java.util.Collection;

import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.EntityCondition;
import com.sprite.framework.entity.model.ModelEntityView;


/**
 * <p>连接操作</p>
 * AND, OR
 * @author Jack
 *
 */
public class EntityJoinOperator extends EntityComparisonOperator<EntityCondition, EntityCondition>{

	public EntityJoinOperator(int id, String code) {
		super(id, code);
	}
	
	public void makeScript(DataScriptStatement script, Collection<? extends EntityCondition> conditions, ModelEntityView modelViewEntity) throws EntityException{
		if(conditions != null && !conditions.isEmpty()){
			int length = conditions.size();
			script.append("(");
			for(EntityCondition condition : conditions){
				length --;
				condition.makeScript(script, modelViewEntity);
				if(length > 0){
					script.append(getCode());
				}
			}
			script.append(")");
		}
	}

}
