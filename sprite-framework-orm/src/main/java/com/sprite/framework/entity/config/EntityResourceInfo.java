package com.sprite.framework.entity.config;

import com.sprite.framework.config.ResourceException;
import com.sprite.framework.config.ResourceInfo;
import com.sprite.utils.UtilXml;
import org.w3c.dom.Document;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 实体相关资源
 * @author Jack
 */
public class EntityResourceInfo extends ResourceInfo {

    // 实体数据
    private final static List<EntityResourceInfo> entityDatas = new LinkedList<>();

    // 实体模型
    private final static List<EntityResourceInfo> entityModels = new LinkedList<>();

    // key is sqlType
    private final static Map<String ,EntityResourceInfo> fieldTypeDef = new HashMap<>();

    public static void addEntityData(EntityResourceInfo resourceInfo){
        entityDatas.add(resourceInfo);
    }

    public static void addEntityModel(EntityResourceInfo resourceInfo){
        entityModels.add(resourceInfo);
    }

    public static void addFieldType(EntityResourceInfo entityResourceInfo){
        fieldTypeDef.put(entityResourceInfo.getName(), entityResourceInfo);
    }

    public static EntityResourceInfo getFieldType(String sqlType){
        return fieldTypeDef.get(sqlType);
    }

    public static List<EntityResourceInfo> getEntityDatas() {
        return entityDatas;
    }

    public static List<EntityResourceInfo> getEntityModels() {
        return entityModels;
    }

    private String resourceType; // resourceType
    private String name;

    public EntityResourceInfo(String location, String resourceType) {
        this.location = location;
        this.resourceType = resourceType;
    }


    public Document getDocument() throws Exception {
        return UtilXml.readXmlDocument(getStream(), false, null);
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
