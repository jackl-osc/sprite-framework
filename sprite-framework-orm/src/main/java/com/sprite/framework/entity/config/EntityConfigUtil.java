package com.sprite.framework.entity.config;

public final class EntityConfigUtil {

	private volatile static EntityConfig instance;
	
	public static EntityConfig getEntityConfig() {
		return instance;
	}
	
	protected static void setEntityConfig(EntityConfig entityConfig) {
		EntityConfigUtil.instance = entityConfig;
	}
}
