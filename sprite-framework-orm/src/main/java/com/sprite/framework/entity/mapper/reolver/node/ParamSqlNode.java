package com.sprite.framework.entity.mapper.reolver.node;

import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.OgnlUtils;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

/**
 * @author Jack
 */
public class ParamSqlNode implements SqlNode{

	private String expression;
	
	public boolean apply(DynamicContext context) {
		context.append("?");
		Object paramValue = OgnlUtils.evaluateObject(expression, context.getBindings());
		context.addParam(paramValue);
		return true;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

}
