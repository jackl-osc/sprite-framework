package com.sprite.framework.entity;

import com.sprite.framework.exception.GeneralRuntimeException;

/**
 * @author Jack
 *
 */
@SuppressWarnings("serial")
public class EntityException extends GeneralRuntimeException{
	
	public static EntityException getInstance(Throwable cause){
		return new EntityException(cause);
	}
	
	public EntityException() {
		super();
	}

	public EntityException(Throwable nested) {
		super(nested);
	}

	public EntityException(String str) {
		super(str);
	}

	public EntityException(String str, Throwable nested) {
		super(str, nested);
	}
}
