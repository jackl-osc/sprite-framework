package com.sprite.framework.entity.condition;

import java.util.Collection;

import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.model.ModelEntityView;

/**
 * 多个操作值
 * @author Jack
 *
 */
public final class CollectionOperateValue implements MultiOperateValue{
	private Collection<?> values;
	
	public <T> CollectionOperateValue(Collection<T> values){
		this.values = values;
	}
	
	@Override
	public void makeStatement(DataScriptStatement statement,
			ModelEntityView modelViewEntity) throws EntityException {
		int length = values.size();
		for(Object obj : values){
			length --;
			
			if(obj instanceof NoParameterized){
				NoParameterized p = (NoParameterized)obj;
				statement.append(p.safeString());
			}else{
				statement.append(" ? ");
				statement.addParam(obj);
			}
			
			if(length > 0){
				statement.append(",");
			}
		}
	}

	public Collection<?> getValues() {
		return values;
	}

}
