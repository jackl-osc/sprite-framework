package com.sprite.framework.entity.mapper.reolver.node;

import java.util.Iterator;
import java.util.List;

import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.OgnlUtils;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

public class ForeachSqlNode implements SqlNode{

	private String item;
	private String index;
	private String collection;
	private String open;
	private String separator;
	private String close;
	
	private SqlNode content;
	
	private List<String> params;
	
	public boolean apply(DynamicContext context) {
		Iterable<?> list = OgnlUtils.evaluateIterable(collection, context);
		Iterator<?> itr = list.iterator();
		
		context.append(open);
		int i = 0;
		while (itr.hasNext()) {
			context.bind(index, i);
			context.bind(item, itr.next());
			content.apply(context);
			context.append(separator);
		}
		context.append(close);
		return true;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getClose() {
		return close;
	}

	public void setClose(String close) {
		this.close = close;
	}

	public SqlNode getContent() {
		return content;
	}

	public void setContent(SqlNode content) {
		this.content = content;
	}

	public List<String> getParams() {
		return params;
	}

	public void setParams(List<String> params) {
		this.params = params;
	}
	
}
