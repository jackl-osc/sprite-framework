package com.sprite.framework.entity.condition;

import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.EntityCondition;
import com.sprite.framework.entity.model.ModelEntityView;
import com.sprite.utils.UtilMisc;

/**
 * 实体操作表达式
 * @author Jack
 *
 */
public class EntityExpr implements EntityCondition {

	private Object lhs;	// 左值
	private EntityOperator<Object, Object, ?> operator;	// 操作
	private Object rhs;	// 右值

	@Override
	public void makeScript(DataScriptStatement script, ModelEntityView modelViewEntity) throws EntityException {
		operator.makeScript(script, lhs, rhs, modelViewEntity);
	}

	public <L, R> void init(L l, EntityOperator<?, ?, ?> operator, R r){
		if (l == null) {
			throw new IllegalArgumentException("The field name/value cannot be null");
		}
		
		if (operator == null) {
			throw new IllegalArgumentException("The operator argument cannot be null");
		}
		
		if (r == null) {
			throw new IllegalArgumentException("The field name/value cannot be null");
		}
		
		this.lhs = l;
		this.operator = UtilMisc.cast(operator);
		this.rhs = r;
	}

}
