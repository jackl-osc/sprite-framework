package com.sprite.framework.entity.mapper.reolver.parser;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.Parser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.utils.UtilXml;

public class OtherwiseNodeParser implements NodeParser{
	public String tagName() {
		return "otherwise";
	}

	public SqlNode parser(Element element) {
		return Parser.parserMix(UtilXml.childNodeList(element));
	}
}
