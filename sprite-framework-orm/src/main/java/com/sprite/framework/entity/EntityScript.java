package com.sprite.framework.entity;

import java.util.Map;

import com.sprite.framework.entity.script.EntityInsertScript;
import com.sprite.framework.entity.script.EntityUpdateScript;

/**
 * 实体脚本
 * @see com.sprite.framework.entity.script.EntityDeleteScript
 * @see com.sprite.framework.entity.script.EntitySelectScript
 * @see com.sprite.framework.entity.script.EntityInsertScript
 * @see com.sprite.framework.entity.script.EntityUpdateScript
 *
 * @author Jack
 */
public interface EntityScript {

	DataScriptStatement getStatement();

	/**
	 * 更新脚本
	 * @param entityName	实体全名
	 * @param fieldToUpdate	要更新的字段
	 * @param whereCondition	条件
	 * @return 更新脚本
	 */
	static EntityScript update(String entityName, Map<String, Object> fieldToUpdate, EntityCondition whereCondition) {
		EntityUpdateScript updateScript = new EntityUpdateScript(entityName);
		updateScript.addFields(fieldToUpdate);
		updateScript.setWhereCondition(whereCondition);
		return updateScript;
	}

	/**
	 * 插入脚本
	 * @param entityName	实体全名
	 * @param fieldToUpdate	要插入的字段
	 * @return 保存脚本
	 */
	static EntityScript insert(String entityName, Map<String, Object> fieldToUpdate) {
		EntityInsertScript updateScript = new EntityInsertScript(entityName);
		updateScript.addFields(fieldToUpdate);
		return updateScript;
	}
}
