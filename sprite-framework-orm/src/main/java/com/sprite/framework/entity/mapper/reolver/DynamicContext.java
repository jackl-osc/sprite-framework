package com.sprite.framework.entity.mapper.reolver;

import java.util.HashMap;
import java.util.Map;

import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.utils.UtilBeans;

public class DynamicContext extends DataScriptStatement{

	public static final String PARAMER_KEY = "_parameter";
	
	private ContextMap bindings;
	
	private String namespace;
	
	private Object param;
	public DynamicContext(Object param) {
		bindings = new ContextMap(param);
		bindings.put(PARAMER_KEY, param);
	}
	
	public String getNamespace() {
		return namespace;
	}


	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public Object getParam() {
		return param;
	}

	public void setParam(Object param) {
		this.param = param;
	}

	public String toSql(){
		return super.toString();
	}
	
	public Map<String, Object> getBindings(){
		return bindings;
	}
	
	public void bind(String key, Object value){
		bindings.put(key, value);
	}
	
	static class ContextMap extends HashMap<String, Object>{

		/**
		 * 
		 */
		private static final long serialVersionUID = -1616257039542730678L;
		private Object param;
		
		private ContextMap(Object param){
			this.param = param;
		}
		
		@Override
		public Object get(Object key) {
			String strKey = (String) key;
			Object value = super.get(strKey);
			if(value != null){
				return value;
			}
			
			if(param != null){
				try {
					return UtilBeans.getProperty(param, strKey);
				}catch (Exception e) {
				}
			}
			
			return null;
		}
		
		
	}
}
