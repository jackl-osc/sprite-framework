package com.sprite.framework.entity.script;

/**
 * entity script function
 * @author Jack
 *
 */
public abstract class EntityScriptFunction {
	private String code;

	protected EntityScriptFunction(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public String function(String field){
		return getCode()+"("+field+")";
	}
	
	public final static EntityScriptFunction MAX = new EntityScriptFunction("max"){};
	public final static EntityScriptFunction MIN = new EntityScriptFunction("min"){};
	public final static EntityScriptFunction COUNT = new EntityScriptFunction("count"){};
	public final static EntityScriptFunction AVG = new EntityScriptFunction("avg"){};
	public final static EntityScriptFunction SUM = new EntityScriptFunction("sum"){};
}
