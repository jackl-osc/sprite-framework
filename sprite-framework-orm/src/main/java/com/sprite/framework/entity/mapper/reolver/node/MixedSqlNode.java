package com.sprite.framework.entity.mapper.reolver.node;


import java.util.LinkedList;
import java.util.List;

import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

public class MixedSqlNode implements SqlNode{

	private List<SqlNode> nodeList = new LinkedList<SqlNode>();
	
	public void append(SqlNode sqlNode){
		nodeList.add(sqlNode);
	}
	
	public boolean apply(DynamicContext context) {
		boolean isApply = false;
		for(SqlNode node : nodeList){
			if(node.apply(context)){
				isApply = true;
			}
		}
		return isApply;
	}
	
}
