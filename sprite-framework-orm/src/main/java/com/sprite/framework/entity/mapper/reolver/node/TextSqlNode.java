package com.sprite.framework.entity.mapper.reolver.node;

import java.util.LinkedList;
import java.util.List;

import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.OgnlUtils;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

/**
 * @author Jack
 */
public class TextSqlNode implements SqlNode{

	private String sql;
	
	private List<String> params = new LinkedList<>();
	
	public TextSqlNode(String sql){
		this.sql = sql;
	}
	
	public TextSqlNode(String sql, List<String> params){
		this.sql = sql;
		this.params.addAll(params);
	}
	
	public boolean apply(DynamicContext context) {
		if(sql != null){
			context.append(sql);
			for(String param : params){
				context.addParam(OgnlUtils.evaluateObject(param, context.getBindings()));
			}
			return true;
					
		}
		return false;
	}

}
