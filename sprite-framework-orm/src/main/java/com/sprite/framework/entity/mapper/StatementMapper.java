package com.sprite.framework.entity.mapper;

import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.mapper.reolver.DynamicContext;
import com.sprite.framework.entity.mapper.reolver.SqlNode;

public class StatementMapper {

	private SqlNode rootNode;
	private String statementId;
	private String namespace;
	public StatementMapper(SqlNode rootNode, String statementId, String namespace){
		this.rootNode = rootNode;
		this.statementId = statementId;
		this.namespace = namespace;
	}
	
	public DataScriptStatement mapper(Object param){
		DynamicContext context = new DynamicContext(param);
		context.setNamespace(namespace);
		rootNode.apply(context);
		
		DataScriptStatement statement = new DataScriptStatement();
		statement.append(context.toSql());
		statement.addParams(context.getParams());
		return statement;
	}

	public String getStatementId() {
		return statementId;
	}
	
}
