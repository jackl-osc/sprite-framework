package com.sprite.framework.entity.util;

public final class SqlLogUtil {

	public static boolean openLog(){
		return OrmConfig.OPENLOG;
	}

	public static void log(String sql) {
		if(OrmConfig.OPENLOG) {
			System.out.println(sql);
		}
	}
}
