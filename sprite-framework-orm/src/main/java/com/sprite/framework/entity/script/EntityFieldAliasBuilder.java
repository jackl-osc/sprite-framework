package com.sprite.framework.entity.script;

import java.util.List;

import com.sprite.framework.entity.model.ModelEntityView;

/**
 * Entity Field Builder
 * @author Jack
 * 
 * @see com.sprite.framework.entity.script.EntityView
 * @see com.sprite.framework.entity.script.EntityAlias
 * 
 */
public interface EntityFieldAliasBuilder extends ModelEntityView{

	/**
	 * 构建字段别名
	 * @param fieldName 字段名称
	 * @param fieldAlias	字段别名
	 * @param function 函数
	 * @return 字段描述
	 */
	EntityFieldAlias buildFieldAlias(String fieldName, String fieldAlias, EntityScriptFunction function);

	/**
	 * 构建视图所有的实体字段
	 * @return 字段列表
	 */
	List<EntityFieldAlias> buildAllFieldAlias();
}
