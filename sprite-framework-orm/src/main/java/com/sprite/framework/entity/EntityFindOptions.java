package com.sprite.framework.entity;


/**
 * @author Jack
 *
 */
public final class EntityFindOptions {
	private int offset = -1;
	private int limit = -1;
	
	public EntityFindOptions(){}
	
	public EntityFindOptions(int offset, int limit) {
		super();
		this.offset = offset;
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
}
