package com.sprite.framework.entity.config;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sprite.utils.UtilXml;

public class EntityConfigReader {
	
	public void buildEntityConfig(Document document) {
		EntityConfig entityConfig = new EntityConfig();
		Element docElement = document.getDocumentElement();
		if(!"entity-config".equals(docElement.getTagName())){
			throw new IllegalArgumentException("entity config error");
		}

		docElement.normalize();

		List<Element> list = UtilXml.childElementList(docElement, "field-type");


		FieldType fieldType = null;
		for(Element element : list) {
			fieldType = new FieldType();
			fieldType.setLocation(element.getAttribute("location"));
			fieldType.setName(element.getAttribute("name"));
			entityConfig.addFieldType(fieldType);
		}

		EntityConfigUtil.setEntityConfig(entityConfig);
	}
}
