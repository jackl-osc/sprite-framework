package com.sprite.framework.entity.util;

/**
 * ORM相关配置
 */
public class OrmConfig {
	/**
	 * 是否开启SQL日志
	 */
	public static boolean OPENLOG = "true".equals(System.getProperty("sprite.sqllog"));

	/**
	 * 是否缓存Mapper
	 */
	public static boolean CACHED_MAPPER = !"false".equals(System.getProperty("sprite.cached_mapper"));
}
