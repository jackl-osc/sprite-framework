package com.sprite.framework.entity.mapper.reolver.parser;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.Parser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.framework.entity.mapper.reolver.node.IfSqlNode;
import com.sprite.utils.UtilXml;

public class IfNodeParser implements NodeParser{

	public SqlNode parser(Element element) {
		IfSqlNode sqlNode = new IfSqlNode();
		sqlNode.setTest(element.getAttribute("test"));
		sqlNode.setSqlNode(Parser.parserMix(UtilXml.childNodeList(element)));
		
		return sqlNode;
	}

	public String tagName() {
		return "if";
	}

}
