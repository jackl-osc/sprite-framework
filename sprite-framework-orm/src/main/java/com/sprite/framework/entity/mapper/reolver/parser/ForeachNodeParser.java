package com.sprite.framework.entity.mapper.reolver.parser;

import org.w3c.dom.Element;

import com.sprite.framework.entity.mapper.reolver.NodeParser;
import com.sprite.framework.entity.mapper.reolver.Parser;
import com.sprite.framework.entity.mapper.reolver.SqlNode;
import com.sprite.framework.entity.mapper.reolver.node.ForeachSqlNode;
import com.sprite.utils.UtilXml;

public class ForeachNodeParser implements NodeParser{

	public String tagName() {
		return "foreach";
	}

	public SqlNode parser(Element element) {
		ForeachSqlNode sqlNode = new ForeachSqlNode();
		sqlNode.setClose(element.getAttribute("close"));
		sqlNode.setCollection(element.getAttribute("collection"));
		sqlNode.setIndex(element.getAttribute("index"));
		sqlNode.setItem(element.getAttribute("item"));
		sqlNode.setOpen(element.getAttribute("open"));
		sqlNode.setSeparator(element.getAttribute("separator"));
		sqlNode.setContent(Parser.parserMix(UtilXml.childNodeList(element)));
		return sqlNode;
	}

}
