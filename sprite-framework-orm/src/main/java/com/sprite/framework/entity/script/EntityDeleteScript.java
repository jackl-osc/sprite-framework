package com.sprite.framework.entity.script;

import com.sprite.framework.entity.DataScriptStatement;
import com.sprite.framework.entity.EntityCondition;
import com.sprite.framework.entity.EntityException;
import com.sprite.framework.entity.EntityScript;
import com.sprite.framework.entity.model.ModelEntity;
import com.sprite.framework.entity.model.ModelEntityUtil;
import com.sprite.framework.entity.model.ModelEntityView;
import com.sprite.utils.UtilString;

/**
 * 删除脚本
 * @author Jack
 */
public final class EntityDeleteScript implements EntityScript{
	

	private String entityName;
	
	private EntityCondition whereCondition;			// 查询条件

	public EntityDeleteScript(String entityName) {
		this.entityName = entityName;
	}

	protected EntityDeleteScript() {
		
	}
	
	public void setWhereCondition(EntityCondition whereCondition){
		this.whereCondition = whereCondition;
	}

	public void makeStatement(DataScriptStatement statement, ModelEntityView modelViewEntity) throws EntityException{
		ModelEntity modelEntity = ModelEntityUtil.getModelEntity(entityName);
		
		statement.append(UtilString.place("DELETE FROM {} ", modelEntity.getTableName()));
		statement.append(" WHERE ");
		whereCondition.makeScript(statement, modelEntity);
	}
	
	@Override
	public DataScriptStatement getStatement() {
		DataScriptStatement statement = new DataScriptStatement();
		makeStatement(statement, null);
		return statement;
	}

}
