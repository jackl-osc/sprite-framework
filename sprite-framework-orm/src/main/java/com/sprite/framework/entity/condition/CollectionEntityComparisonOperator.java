package com.sprite.framework.entity.condition;

/**
 * <p>集合的比较操作</p>
 * IN, NOT IN
 * @author Jack
 *
 */
public abstract class CollectionEntityComparisonOperator<T extends MultiOperateValue> extends
		EntityOperator<String, T, Boolean> {

	public CollectionEntityComparisonOperator(int id, String code) {
		super(id, code);
	}

}
