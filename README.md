# sprite-framework

#### 介绍
Sprite 是基于Spring 的一套快速开发框架，框架整体划分为framework、component、modules、misc共四类。framework提供项目基础包含对数据层的访问、项目的启动、web项目容器等，component层提供丰富的系统组件，例如缓存、文件上传等，modules 层提供一些功能模块，这些模块一般都是需要与数据库交互的，misc层提供了一些工具类。

[文档说明](http://sprite.zidot.cn) 持续更新中。。。

