package com.sprite.framework.config;

import com.sprite.framework.exception.GeneralException;

/**
 * @author Jack
 *
 */
@SuppressWarnings("serial")
public class ResourceException extends GeneralException{
	public ResourceException() {
		super();
	}

	public ResourceException(String str) {
		super(str);
	}

	public ResourceException(Throwable nested) {
		super(nested);
	}

	public ResourceException(String str, Throwable nested) {
		super(str, nested);
	}
}
