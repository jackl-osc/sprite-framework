package com.sprite.framework.config;

import java.io.InputStream;

/**
 * @author Jack
 *
 */
public interface ResourceHandler {
	 String getLocation();
	 
	 InputStream getStream() throws ResourceException;
}
