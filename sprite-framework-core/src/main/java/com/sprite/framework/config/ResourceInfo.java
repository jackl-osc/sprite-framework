package com.sprite.framework.config;

import com.sprite.utils.UtilURL;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * 资源描述
 * @author Jack
 */
public abstract class ResourceInfo implements ResourceHandler {

    protected String location;

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public InputStream getStream() throws ResourceException {
        URL url = UtilURL.fromResource(location);
        if(url == null) return null;

        try {
            return url.openStream();
        } catch (IOException e) {
            throw new ResourceException(e);
        }
    }
}
