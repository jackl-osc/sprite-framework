package com.sprite.framework.common;


import java.io.Serializable;
import java.util.Map;

import com.sprite.utils.UtilMisc;

/**
 * @author Jack
 *
 */
public class Result implements Serializable {
	private static final long serialVersionUID = -4226358100664011147L;

	public static final String CODE_SUCCESS = "0";
	public static final String CODE_ERROR = "1";
	public static final String CODE_SYSTEM_ERROR= "50000";
	public static final String CODE_AUTH_ERROR= "40100"; // 权限问题
	public static final String CODE_AUTH_LOGIN= "40101"; // 未登录
	
	public static final Result SUCCESS = new ResultReadonly(CODE_SUCCESS, "成功响应");
	public static final Result FAIL = new ResultReadonly(CODE_ERROR, "");
	public static final Result SYSTEM_ERROR = new ResultReadonly(CODE_SYSTEM_ERROR, "系统出错了");
	public static final Result AUTH_ERROR = new ResultReadonly(CODE_AUTH_ERROR, "权限问题");
	public static final Result AUTH_LOGIN = new ResultReadonly(CODE_AUTH_LOGIN, "未登录");
	
	protected String code; 	// 错误码
	protected String msg; 		// 信息
	protected Object data; 		// 数据

	public Result() {

	}

	public Result(String errCode) {
		this(errCode, null, null);
	}
	
	public Result(Object data) {
		this(CODE_SUCCESS, null, data);
	}

	public Result(String errCode, String errMsg, Object data) {
		this.code = errCode;
		this.msg = errMsg;
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public Map<String, Object> toMap(){
		return UtilMisc.toMap("code", code, "msg", msg, "data", data);
	}
	
	public static Result instance(String code, String msg){
		return new Result(code, msg, null);
	}
	
	public static Result success( Object data){
		return new Result(CODE_SUCCESS, null, data);
	}
	
	private static class  ResultReadonly extends Result{

		private static final long serialVersionUID = -216838646255149956L;
		
		public ResultReadonly(String errCode, String errMsg) {
			super(errCode, errMsg, null);
		}
		
		public void setCode(String errCode) {
			throw new UnsupportedOperationException("The Object is readonly");
		}

		public void setMsg(String errMsg) {
			throw new UnsupportedOperationException("The Object is readonly");
		}
		
		public void setData(Object data) {
			throw new UnsupportedOperationException("The Object is readonly");
		}
	}
	
}
