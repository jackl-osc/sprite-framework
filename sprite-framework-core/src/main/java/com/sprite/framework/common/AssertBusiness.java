package com.sprite.framework.common;

import java.util.Collection;

import com.sprite.framework.exception.IllegalArgumentBusinessException;
import com.sprite.framework.exception.MustArgumentBusinessException;

/**
 * <p>业务断言工具类</p>
 * @author Jack
 * @version 1.0
 * 
 */
public final class AssertBusiness {

	/**
	 * <p>真值检测，抛出  {@link IllegalArgumentBusinessException}</p>
	 * @param express 布尔表达式
	 * @param message 错误消息
	 */
	public static void isTrue(boolean express, String message){
		if(!express){
			throw IllegalArgumentBusinessException.getInstance(message);
		}
	}

	
	/**
	 * <p>真值检测，抛出  {@link IllegalArgumentBusinessException}</p>
	 * @param express 布尔表达式
	 * @param message 错误消息
	 */
	public static void isFalse(boolean express, String message){
		if(express){
			throw IllegalArgumentBusinessException.getInstance(message);
		}
	}
	
	/**
	 * <p>非空检测，抛出  {@link MustArgumentBusinessException}</p>
	 * @param param 对象
	 * @param message 错误消息
	 */
	public static void notNull(Object param, String message){
		if(param == null){
			throw MustArgumentBusinessException.getInstance(message);
		}
	}
	
	/**
	 * <p>非空字符检测，抛出  {@link MustArgumentBusinessException}</p>
	 * @param param 字符串
	 * @param message 错误消息
	 */
	public static void notBlank(String param, String message){
		if(param == null || "".equals(param.trim())){
			throw MustArgumentBusinessException.getInstance(message);
		}
	}

	/**
	 * <p>非空集合检测，抛出  {@link MustArgumentBusinessException}</p>
	 * @param collection 集合
	 * @param message 错误消息
	 */
	public static void notEmpty(Collection<?> collection, String message){
		if(collection == null || collection.isEmpty()){
			throw MustArgumentBusinessException.getInstance(message);
		}
	}

	private AssertBusiness(){}
}
