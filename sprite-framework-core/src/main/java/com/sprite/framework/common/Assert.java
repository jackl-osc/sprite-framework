package com.sprite.framework.common;

/**
 * 用于校验参数
 * @author LuMengwei
 * @version v1.0
 *
 */
public final class Assert {
	
	public static void isTrue(boolean expression, String message) {
		if (!expression) {
			throw new IllegalArgumentException(message);
		}
	}
	
	public static void isFalse(boolean expression,String message){
		if (expression) {
			throw new IllegalArgumentException(message);
		}
	}
	
	public static void isNull(Object expression,String message){
		if(expression != null){
			throw new IllegalArgumentException(message);
		}
	}
	
	public static void notNull(Object expression,String message){
		if(expression == null){
			throw new IllegalArgumentException(message);
		}
	}
}
