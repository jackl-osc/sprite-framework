package com.sprite.framework.common;

import java.util.List;

/**
 * @author Jack
 *
 */
public class DataList<T> {
	private int offset;
	private int limit;
	private long totalCount;
	private int listCount;
	private List<T> list; 				// 数据
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	
	public int getListCount() {
		return listCount;
	}
	public void setListCount(int listCount) {
		this.listCount = listCount;
	}
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
		listCount = list!= null?list.size():0;
	}
	
	
}
