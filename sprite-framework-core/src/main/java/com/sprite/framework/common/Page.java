package com.sprite.framework.common;

import java.util.List;

/**
 * @author Jack
 *
 * @param <T>
 */
public class Page<T> {
	private int 	pageNo; 			// 当前页
	private int 	pageSize; 			// 每页记录数
	private long 	totalCount; 		// 记录总数
	private List<T> dataList; 				// 数据

	public Page(int pageSize) {
		this.pageSize = pageSize;
	}

	public Page(int pageNo, int pageSize) {
		this.pageNo = pageNo;
		this.pageSize = pageSize;
	}

	public Page(List<T> list, int pageNo, int pageSize, long totalCount) {
		this.dataList = list;
		this.pageNo = pageNo;
		this.totalCount = totalCount;
		setRow(pageSize);
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	private void setRow(int pageSize) {
		if (pageSize <= 0) {
			this.pageSize = 15;
		} else {
			this.pageSize = pageSize;
		}
	}
	
	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}

	
	public <K> Page<K> populatePage(){
		Page<K> page = new Page<>(this.pageNo, this.pageSize);
		return page;
	}
	
	public static <T> Page<T> getInstance(int page, int pageSize, long totalCount, List<T> list) {
		Page<T> result = new Page<T>(pageSize);
		result.setDataList(list);
		result.setTotalCount(totalCount);

		return result;
	}

	public int getFirstResult() {
		if (pageNo == 0 || pageSize == 0) {
			return 0;
		}
		return (pageNo - 1) * pageSize;
	}
}
