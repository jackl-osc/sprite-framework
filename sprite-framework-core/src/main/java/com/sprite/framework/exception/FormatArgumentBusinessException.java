package com.sprite.framework.exception;

/**
 * <p>参数格式化错误</p>
 * @author Jack
 *
 */
public final class FormatArgumentBusinessException extends IllegalArgumentBusinessException{

	private static final long serialVersionUID = -4972008668434065656L;

	public FormatArgumentBusinessException(String errMsg) {
		super("11", errMsg != null ?errMsg:"非法的格式");
	}

}
