package com.sprite.framework.exception;

/**
 * <p>编码异常类</p>
 * @author Jack
 * @version 1.0
 * 
 */
public final class SystemBusinessException extends BusinessException{

	private static final long serialVersionUID = 4596452757864465484L;

	private SystemBusinessException(String errMsg,Throwable cause) {
		super("50000", errMsg != null ?errMsg:"系统异常", cause);
	}

	public static SystemBusinessException getInstance(String errMsg,Throwable cause){
		return new SystemBusinessException(errMsg, cause);
	}
}
