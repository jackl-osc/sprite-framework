package com.sprite.framework.exception;

import java.util.List;

@SuppressWarnings("serial")
public class GeneralException extends Exception{
	List<String> messages ;

	public GeneralException() {
		super();
	}

	/**
	 * Constructs an <code>GeneralException</code> with the specified detail message.
	 * @param msg the detail message.
	 */
	public GeneralException(String msg) {
		super(msg);
	}

	/**
	 * Constructs an <code>GeneralException</code> with the specified detail message and nested Exception.
	 * @param msg the detail message.
	 * @param nested the nested exception.
	 */
	public GeneralException(String msg, Throwable nested) {
		super(msg, nested);
	}

	/**
	 * Constructs an <code>GeneralException</code> with the specified detail message and nested Exception.
	 * @param nested the nested exception.
	 */
	public GeneralException(Throwable nested) {
		super(nested);
	}

	/**
	 * Constructs an <code>GeneralException</code> with the specified detail message, list and nested Exception.
	 * @param msg the detail message.
	 * @param messages error message list.
	 */
	public GeneralException(String msg, List<String> messages) {
		super(msg);
		this.messages = messages;
	}

	/**
	 * Constructs an <code>GeneralException</code> with the specified detail message, list and nested Exception.
	 * @param msg the detail message.
	 * @param messages error message list.
	 * @param nested the nexted exception
	 */
	public GeneralException(String msg, List<String> messages, Throwable nested) {
		super(msg, nested);
		this.messages = messages;
	}

	/**
	 * Constructs an <code>GeneralException</code> with the specified detail message list and nested Exception.
	 * @param messages error message list.
	 * @param nested the nested exception.
	 */
	public GeneralException(List<String> messages, Throwable nested) {
		super(nested);
		this.messages = messages;
	}

	public GeneralException(List<String> messages) {
		super();
		this.messages = messages;
	}
	
    /** Returns the detail message, including the message from the nested exception if there is one. */
    @Override
    public String getMessage() {
        Throwable nested = getCause();
        if (nested != null) {
            if (super.getMessage() == null) {
                return nested.getMessage();
            } else {
                return super.getMessage() + " (" + nested.getMessage() + ")";
            }
        } else {
            return super.getMessage();
        }
    }
}
