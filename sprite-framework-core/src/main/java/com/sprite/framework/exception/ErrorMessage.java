package com.sprite.framework.exception;

import java.util.LinkedList;
import java.util.List;

public final class ErrorMessage {

	private List<String> messageList = new LinkedList<>();
	
	public void addMessage(String message) {
		messageList.add(message);
	}
	
	public boolean isEmpty() {
		return messageList.isEmpty();
	}

	public String getMutilMessage() {
		StringBuilder stringBuilder = new StringBuilder();
		for(String message: messageList) {
			stringBuilder.append(message).append(";\n");
		}
		
		return stringBuilder.toString();
	}
	
	@Override
	public String toString() {
		if(isEmpty()) {
			return "Empty ErrorMessge";
		}
		
		return getMutilMessage();
	}
	
}
