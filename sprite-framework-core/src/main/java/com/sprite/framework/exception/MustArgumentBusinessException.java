package com.sprite.framework.exception;

/**
 * <p>参数缺失</p>
 * @author Jack
 * @version 1.0
 * 
 */
public final class MustArgumentBusinessException extends IllegalArgumentBusinessException{

	private static final long serialVersionUID = -4972008668434065656L;

	public MustArgumentBusinessException(String errMsg) {
		super("02",errMsg);
	}

	public static MustArgumentBusinessException getInstance(String message){
		return new MustArgumentBusinessException(message);
	}
}
