package com.sprite.framework.exception;

/**
 * @author Jack
 *
 */
@SuppressWarnings("serial")
public class GeneralRuntimeException extends RuntimeException{
	Throwable nested = null;
	
	public GeneralRuntimeException() {
		super();
	}

	public GeneralRuntimeException(String message) {
		super(message);
	}

	public GeneralRuntimeException(Throwable nested) {
		super(nested);
		this.nested = nested;
	}
	
	public GeneralRuntimeException(String message, Throwable nested) {
		super(message);
		this.nested = nested;
	}
}
