package com.sprite.framework.exception;

/**
 * <p>业务类异常</p>
 * 此类异常由服务层抛出
 * @author Jack
 * @version 1.0
 *
 */
public class BusinessException extends RuntimeException{
	private String errCode;		// 异常码
	private String errMsg;		// 异常信息
	
	private static final long serialVersionUID = -2723627474269384044L;
	
	public BusinessException(String errCode, String errMsg) {
		this(errCode, errMsg, null);
	}
	
	public BusinessException(String errCode, String errMsg,Throwable cause) {
		super(errMsg,cause);
		checkErrCode(errCode);
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	private void checkErrCode(String errCode){
		if(errCode != null && errCode.length() == 5){
			Integer.valueOf(errCode);
			return ;
		}
		
		throw new IllegalArgumentException("Invalid error code , the error code consist of 5 digits");
	}
	
	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
