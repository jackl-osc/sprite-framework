package com.sprite.framework.exception;

/**
 * <p>非法的参数异常</p>
 *	调用服务层方法时，如果参数不和法会抛出此类异常
 * @author Jack
 * @version 1.0
 * 
 */
public class IllegalArgumentBusinessException extends BusinessException {

	private static final long serialVersionUID = -3212662180571853813L;
	public IllegalArgumentBusinessException(String errMsg){
		this("00", errMsg);
	}
	
	public IllegalArgumentBusinessException(String errCode, String errMsg) {
		super("403" + errCode,errMsg != null?errMsg:"非法参数");
	}
	
	public static IllegalArgumentBusinessException getInstance(String message){
		return new IllegalArgumentBusinessException(message);
	}
}
