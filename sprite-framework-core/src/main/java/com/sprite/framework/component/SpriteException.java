package com.sprite.framework.component;

import com.sprite.framework.exception.GeneralException;

/**
 * @author Jack
 *
 */
@SuppressWarnings("serial")
public class SpriteException extends GeneralException {

	public SpriteException() {
		super();
	}

	public SpriteException(String str) {
		super(str);
	}

	public SpriteException(Throwable nested) {
		super(nested);
	}

	public SpriteException(String str, Throwable nested) {
		super(str, nested);
	}
}
