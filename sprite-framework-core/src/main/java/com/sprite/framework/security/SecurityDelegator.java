package com.sprite.framework.security;

import java.util.List;

/**
 * <p>权限描述</p>
 * 包含数据权限
 * @author Jack
 *
 */
public interface SecurityDelegator {
	
	boolean hasTenant(String tenantId);
	
	boolean hasPermission(String permission);

	boolean hasPermissionGroup(String group);
	
	String getPartyId();

	List<String> getCanReadGroup();

	void resetGroupIds(List<String> groupIds);
	
	void setChiefGroupId(String groupId);

	String getChiefGroupIdAssert();

	List<String> getGroupIds();
	
}
