package com.sprite.framework.entity;

/**
 * @author Jack
 *
 */
public abstract class SequencedEntityLogiclyDeletable extends SequencedEntity implements LogiclyDeletable{
	private boolean deleted;

	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}
