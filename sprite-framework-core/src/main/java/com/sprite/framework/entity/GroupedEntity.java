package com.sprite.framework.entity;

/**
 * @author Jack
 *
 */
public interface GroupedEntity extends IdentifyEntity{
	String getGroupId();
	void setGroupId(String groupId);
}
