package com.sprite.framework.entity;


/**
 * @author Jack
 *
 */
public interface IdentifyEntity {
	Long getCreatedStamp();
	void setCreatedStamp(Long stamp);
	
	Long getUpdatedStamp();
	void setUpdatedStamp(Long stamp);
	
	String getCreatedByParty();
	void setCreatedByParty(String partyId);
	
	
	String getUpdatedByParty();
	void setUpdatedByParty(String partyId);
}
