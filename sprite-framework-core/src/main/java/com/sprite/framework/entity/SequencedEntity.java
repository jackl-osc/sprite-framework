package com.sprite.framework.entity;

public abstract class SequencedEntity extends AbstractEntity{
	private String id;

	public final String getId() {
		return id;
	}

	public final void setId(String id) {
		this.id = id;
	}
}
