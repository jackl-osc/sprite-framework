package com.sprite.framework.entity;

/**
 * @author Jack
 *
 */
public interface TenantEntity {
	String getTenantId() ;
	void setTenantId(String tenantId);
}
