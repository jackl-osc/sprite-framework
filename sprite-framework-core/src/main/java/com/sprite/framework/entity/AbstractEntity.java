package com.sprite.framework.entity;

/**
 * <p>实体类抽象实现</p>
 * @author Jack
 * @version v1.0
 * 
 */
public abstract class AbstractEntity implements IdentifyEntity{
	private Long createdStamp;		// 创建时间
	private Long updateStamp;		// 更新时间
	private String createdByParty;	// 创建人
	private String updatedByParty;	// 更新人
	
	public Long getCreatedStamp(){
		return createdStamp;
	}
	
	public void setCreatedStamp(Long stamp){
		createdStamp = stamp;
	}
	
	public Long getUpdatedStamp(){
		return updateStamp;
	}
	public void setUpdatedStamp(Long stamp){
		updateStamp = stamp;
	}
	
	public String getCreatedByParty(){
		return createdByParty;
	}
	public void setCreatedByParty(String partyId){
		createdByParty = partyId;
	}
	
	
	public String getUpdatedByParty(){
		return updatedByParty;
	}
	public void setUpdatedByParty(String partyId){
		updatedByParty = partyId;
	}
}
