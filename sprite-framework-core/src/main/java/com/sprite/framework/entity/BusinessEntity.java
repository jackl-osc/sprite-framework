package com.sprite.framework.entity;

/**
 * @author Jack
 *
 */
public class BusinessEntity extends SequencedEntity implements GroupedEntity, LogiclyDeletable{
	private String groupId;
	private String tenantId;
	private boolean deleted;
	
	@Override
	public String getGroupId() {
		return groupId;
	}

	@Override
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}
