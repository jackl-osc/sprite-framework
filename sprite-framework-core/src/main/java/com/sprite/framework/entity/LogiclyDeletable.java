package com.sprite.framework.entity;

/**
 * 逻辑删除标识接口
 * @author Jack
 *
 */
public interface LogiclyDeletable extends IdentifyEntity{
	boolean isDeleted();
	void setDeleted(boolean deleted);
}
