package com.sprite.framework.bone.config;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author Jack
 * @since 2.0.1
 */
public class JdbcConfig {

    @Value("${jdbc.driver}")
    private String jdbcDriverClass;

    @Value("${jdbc.url}")
    private String jdbcUrl;

    @Value("${jdbc.username}")
    private String username;

    @Value("${jdbc.password}")
    private String password;

    @Value("${jdbc.initialSize:10}")
    private int initialSize;    // 初始化连接大小

    @Value("${jdbc.maxTotal:20}")
    private int maxTotal;   // 连接池最大使用连接数量

    @Value("${jdbc.maxIdle:10}")
    private int maxIdle;    // 连接池最大空闲

    @Value("${jdbc.minIdle:5}")
    private int minIdle;    // 连接池最小空闲

    @Value("${jdbc.maxWaitMillis:6000}")
    private int maxWaitMillis;      // 获取连接最大等待时间

    @Value("${jdbc.testOnReturn:false}")
    private boolean testOnReturn;

    @Value("${jdbc.testWhileIdle:true}")
    private boolean testWhileIdle;

    @Value("${jdbc.timeBetweenEvictionRunsMillis:6000}")
    private int timeBetweenEvictionRunsMillis;  // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒

    @Value("${jdbc.minEvictableIdleTimeMillis:25200000}")
    private int minEvictableIdleTimeMillis; // 配置一个连接在池中最小生存的时间，单位是毫秒

    @Value("${jdbc.removeAbandonedTimeout:1800}")
    private int removeAbandonedTimeout; // 1800秒，也就是30分钟

    @Value("${jdbc.logAbandoned:true}")
    private boolean logAbandoned;   // 闭abanded连接时输出错误日志

    public String getJdbcDriverClass() {
        return jdbcDriverClass;
    }

    public void setJdbcDriverClass(String jdbcDriverClass) {
        this.jdbcDriverClass = jdbcDriverClass;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(int initialSize) {
        this.initialSize = initialSize;
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(int minIdle) {
        this.minIdle = minIdle;
    }

    public int getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(int maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    public boolean isTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public boolean isTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public int getTimeBetweenEvictionRunsMillis() {
        return timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public int getMinEvictableIdleTimeMillis() {
        return minEvictableIdleTimeMillis;
    }

    public void setMinEvictableIdleTimeMillis(int minEvictableIdleTimeMillis) {
        this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
    }

    public int getRemoveAbandonedTimeout() {
        return removeAbandonedTimeout;
    }

    public void setRemoveAbandonedTimeout(int removeAbandonedTimeout) {
        this.removeAbandonedTimeout = removeAbandonedTimeout;
    }

    public boolean isLogAbandoned() {
        return logAbandoned;
    }

    public void setLogAbandoned(boolean logAbandoned) {
        this.logAbandoned = logAbandoned;
    }
}
