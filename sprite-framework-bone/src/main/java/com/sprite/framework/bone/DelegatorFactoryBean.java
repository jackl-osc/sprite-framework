package com.sprite.framework.bone;

import com.sprite.framework.entity.*;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Jack
 *
 */
public class DelegatorFactoryBean implements FactoryBean<Delegator>{
	
	@Autowired(required = false)
	private EntityInterceptor entityInterceptor;
	
	@Autowired
	private EntityScriptExecutor entityScriptExecutor;

	@Autowired(required = false)
	private List<IdGenerator> generatorList;
	
	public EntityInterceptor getEntityInterceptor() {
		return entityInterceptor;
	}

	public void setEntityInterceptor(EntityInterceptor entityInterceptor) {
		this.entityInterceptor = entityInterceptor;
	}

	private GenericDelegator delegator;

	@Override
	public Delegator getObject() throws Exception {
		if(delegator == null){
			delegator = new GenericDelegator();
			delegator.setEntityInterceptor(entityInterceptor);
			delegator.setScriptExecutor(entityScriptExecutor);
			delegator.addGenerator(generatorList);
		}

		return delegator;
	}

	@Override
	public Class<?> getObjectType() {
		return Delegator.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
